#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
int main() {
	int first_num, second_num, min, max, cycle_count, res = 0;
	printf("Enter first number\n");
	scanf("%d", &first_num);	//Ввод пользователем первого числа
	printf("Enter second number\n");
	scanf("%d", &second_num);     	//Ввод пользователем второго числа
	if (first_num > second_num) {  	//Сортировка чисел на большее и меньшее 
		max = first_num;
		min = second_num;
	} else {
		max = second_num;
		min = first_num;
	}
	cycle_count = (max - min);	//Определение количества циклов
	for(int i = 0; i <= cycle_count; i++) {		//Нахождение суммы чисел между заданными числами 
		res += min + i;		
	}
	printf("The result of adding numbers in between %d and %d is %d\n", min, max, res);	//Вывод результата
	return 0;
}
