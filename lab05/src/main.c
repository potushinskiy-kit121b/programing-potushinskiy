#include <stdio.h>
int findOdd_or_even(int number)
{
	for (int i = 2; i <= number / 2; i++) {
		if (number % i == 0)
			return 1;
	}

	return 0;
}

int main()
{
	int number = 7573;
	int odd_or_even = findOdd_or_even(number); //1 - введене число парне, 0 - не парне
	return 0;
}
