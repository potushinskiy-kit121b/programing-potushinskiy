#include <stdio.h>
#include <string.h>
/** Функция для поиска количества слов в предложении */
int StrWordLen(char str[])
{
	char q[] = " "; /** Масив содержащий "пустую" строку, для нахождения пробеллов в других масивах */
	int wordCount = 0;
	for (int i = 0; i < strlen(str); i++) /** Поиск пробела */
		if (str[i] != q[0]) {
			wordCount++;
			for (; i < strlen(str); i++) /** Поиск следующего слова */
				if (str[i] == q[0])
					break;
		}
	return wordCount;
}
/** Функция для определения четности числа */
int findOdd_or_even(int number)
{
	for (int i = 2; i <= number / 2; i++) {
		if (number % i ==
		    0) /** Если число поделиться нацело на i хоть в одной из итерраций, то єто значит что оно сложное, в противном случае простое  */
			return 1;
	}

	return 0;
}

int main()
{
	char str[] =
		"У Курта Кобейна в компьютере было много винчестеров, но пользовался он только одним"; /** Входящее предложение для примера, для полноценной работы строка получаеться от пользователя или другой части кода */
	int wordCount = StrWordLen(str);
	int odd_even = findOdd_or_even(wordCount);
	return 0;
}
