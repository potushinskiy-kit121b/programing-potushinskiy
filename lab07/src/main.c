#include <stdio.h>
#include <string.h>
int StrWordLen(char str[])
{
	char q[] = " ";
	int wordCount = 0;
	for (int i = 0; i < strlen(str); i++) //Поиск пробела
		if (str[i] != q[0]) {
			wordCount++;
			for (; i < strlen(str); i++) //Поиск следующего слова
				if (str[i] == q[0])
					break;
		}
	return wordCount;
}
int findOdd_or_even(int number)
{
	for (int i = 2; i <= number / 2; i++) {
		if (number % i == 0)
			return 1;
	}

	return 0;
}

int main()
{
	char str[] = "У Курта Кобейна в компьютере было много винчестеров, но пользовался он только одним";
	int wordCount = StrWordLen(str);
	int odd_even = findOdd_or_even(wordCount);
	return 0;
}
