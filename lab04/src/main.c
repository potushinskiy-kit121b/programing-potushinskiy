#include <stdio.h>
int findCent(int year) {
  int cent = 0, b, c;
  if (year <= 100)
    return cent = 1;
  b = year / 100;
  c = year - b * 100;
  if (c > 0)	//Перевірка чи більше нуля права частина числа
    cent = 1;
  else
    cent = 0;
  cent += b;
  return cent;
}

int main() {
  int year = 2021;
  int centure = findCent(year);
  return 0;
}
